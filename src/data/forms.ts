import { FormRowProps } from '../components/tables/interfaces';

export const formsData: Array<FormRowProps> = [
  {
    id: 12312451,
    name: 'Intro to Fertility Treatment',
    signers: [
      {
        id: '1231241',
        name: 'Barbara McClintock',
        initials: 'BM',
        imgSrc: '/images/test-avatar-patient-01.png',
      },
      { id: '2412341', name: 'Abdul McClintock', initials: 'AM' },
      { id: '2422348', name: 'Cole Coleman', initials: 'CC' },
      { id: '2416341', name: 'Blair Doe', initials: 'AM' },
    ],
    activeSigner: '2422348',
    dueDate: new Date('October 31, 2020'),
    remindedDate: new Date('October 1, 2020'),
  },
];
