import { PatientRowProps } from '../components/tables/interfaces';

export const patientsData: Array<PatientRowProps> = [
  {
    primary: {
      id: '098348376',
      name: 'Barbara McClintock',
      imgSrc: '/images/test-avatar-patient-01.png',
      initials: 'BM',
    },
    secondary: { id: '09834423', name: 'Abdul McClintock', initials: 'AM' },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [
        { count: 3, type: 'module', status: 'complete' },
        { count: 2, type: 'form', status: 'incomplete' },
        { count: 5, type: 'module', status: 'overdue' },
      ],
      timeStamp: new Date('September 28, 2020 03:24:00'),
    },
  },

  {
    primary: {
      id: '098348375',
      name: 'Calvin Blackman Bridges',
      initials: 'CBB',
    },
    secondary: { id: '09834422', name: 'Randy Smith', initials: 'RS' },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [
        { count: 1, type: 'module', status: 'complete' },
        { count: 2, type: 'form', status: 'incomplete' },
      ],
      timeStamp: new Date('October 5, 2020 03:24:00'),
    },
  },

  {
    primary: {
      id: '0983481234',
      name: 'Allison Braun',
      imgSrc: '/images/test-avatar-patient-03.png',
      initials: 'AB',
    },
    secondary: { id: '09854423', name: 'Peter Bankhead', initials: 'PB' },
    doctor: { id: '412351244', name: 'Dorothy Dame', initials: 'DD' },
    status: {
      items: [
        { count: 3, type: 'module', status: 'incomplete' },
        { count: 2, type: 'form', status: 'incomplete' },
      ],
      timeStamp: new Date('October 3, 2020 03:24:00'),
    },
  },

  {
    primary: {
      id: '0983498234',
      name: 'Alfonso Collen',
      imgSrc: '/images/test-avatar-patient-04.png',
      initials: 'AC',
    },
    secondary: { id: '09854423', name: 'Cristofer Colhane', initials: 'CC' },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [{ count: 3, type: 'module', status: 'complete' }],
      timeStamp: new Date('October 7, 2020 03:24:00'),
    },
  },

  {
    primary: { id: '1983498234', name: 'Makenna Stanton', initials: 'MS' },
    secondary: { id: '09854423', name: 'Nolan Stanton', initials: 'NS' },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [
        { count: 1, type: 'module', status: 'complete' },
        { count: 2, type: 'form', status: 'incomplete' },
      ],
      timeStamp: new Date('October 7, 2020 03:24:00'),
    },
  },

  {
    primary: {
      id: '2983498234',
      name: 'Sarah Jane McKinnley',
      imgSrc: '/images/test-avatar-patient-05.png',
      initials: 'SJM',
    },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [{ count: 4, type: 'module', status: 'complete' }],
      timeStamp: new Date('October 8, 2020 11:24:00'),
    },
  },

  {
    primary: {
      id: '5983498234',
      name: 'Lydia Botosh',
      imgSrc: '/images/test-avatar-patient-06.png',
      initials: 'LB',
    },
    doctor: { id: '412351324', name: 'Charlie Curtis', initials: 'CC' },
    status: {
      items: [{ count: 4, type: 'module', status: 'complete' }],
      timeStamp: new Date('October 8, 2020 09:24:00'),
    },
  },
];
