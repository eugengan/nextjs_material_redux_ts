import React, { ReactElement } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import NavItem from './common/NavItem';

import HomeIcon from './common/icons/HomeFilled';
import LibraryIcon from '@material-ui/icons/LocalLibraryOutlined';
import PracticeIcon from '@material-ui/icons/Domain';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';

const drawerWidth = 260;

const useStyles = makeStyles(({ spacing, typography }: Theme) =>
  createStyles({
    drawerPaper: {
      width: drawerWidth,
      backgroundColor: '#F2F6FB',
      display: 'flex',
      flexDirection: 'column',
    },
    logo: {
      width: 170,
      marginTop: 34,
      marginLeft: 24,
      marginBottom: 48,
    },
    avatar: {
      marginBottom: spacing(1),
    },
    drawerContent: {
      padding: '0 40px',
    },
    profileHolder: {
      marginBottom: 40,
    },
    navList: {
      marginLeft: -5,
    },
    footer: {
      position: 'absolute',
      bottom: spacing(4),
      left: spacing(5),
      color: '#B3C4CD',
      fontSize: typography.pxToRem(13),
    },
    footerLink: {
      textDecoration: 'underline',
    },
  })
);

export default function MainNav(): ReactElement {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Drawer
        variant="permanent"
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <img src="/images/logo.svg" alt="EngagedMD Logo" className={classes.logo} />
        <div className={classes.drawerContent}>
          <div className={classes.profileHolder}>
            <Avatar
              alt="Charlie Curtis"
              src="/images/test-avatar-doctor-01.png"
              className={classes.avatar}
            />
            <Typography variant="subtitle2">Charlie Curtis</Typography>
            <Typography variant="body2">Brookfiled Fertility</Typography>
          </div>
          <List className={classes.navList}>
            <NavItem link="/" icon={<HomeIcon />} text="Home" />
            <NavItem link="/library" icon={<LibraryIcon />} text="Library" />
            <NavItem link="/practice" icon={<PracticeIcon />} text="Practice" />
          </List>
        </div>
        <div className={classes.footer}>
          <Link href="#" color="textSecondary" className={classes.footerLink}>
            Privacy Policy
          </Link>{' '}
          •{' '}
          <Link href="#" color="textSecondary" className={classes.footerLink}>
            Terms of Use
          </Link>
        </div>
      </Drawer>
    </React.Fragment>
  );
}
