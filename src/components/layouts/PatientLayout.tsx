import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import StyledTabs from '../common/StyledTabs';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import SwitchIcon from '../common/icons/Switch';
import ProfileItem from '../common/ProfileItem';

const useStyles = makeStyles(({ spacing, palette }: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginTop: -spacing(7),
    },
    patientHeader: {
      display: 'flex',
      alignItems: 'center',
      boxShadow: `inset 0 -1px 1px ${fade(palette.text.primary, 0.1)}`,
      padding: `11px ${spacing(3)}px`,
      marginLeft: -spacing(6),
    },
    backButton: {
      marginRight: spacing(3),
    },
    switchButton: {
      opacity: 0.3,
      margin: `0 ${spacing(3)}px`,
    },
  })
);

export default function HomeLayout({
  children,
}: {
  children: JSX.Element[] | JSX.Element;
}): ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.patientHeader}>
        <IconButton className={classes.backButton} href="/patients">
          <ArrowBackIcon />
        </IconButton>
        <ProfileItem
          avatar={{
            src: '/images/test-avatar-patient-01.png',
            initials: 'BM',
          }}
          name="Barbara McClintock"
          id="213412351"
        />
        <IconButton className={classes.switchButton}>
          <SwitchIcon />
        </IconButton>
        <ProfileItem
          avatar={{
            src: '',
            initials: 'AM',
          }}
          name="Adam McClintock"
          id="213462351"
        />
      </div>
      <StyledTabs
        links={[
          { name: 'Content', route: '/patient' },
          { name: 'Info', route: '/patient/info' },
        ]}
      />
      {children}
    </div>
  );
}
