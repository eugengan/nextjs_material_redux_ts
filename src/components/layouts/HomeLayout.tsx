import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import StyledTabs from '../common/StyledTabs';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

export default function HomeLayout({
  children,
}: {
  children: JSX.Element[] | JSX.Element;
}): ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <StyledTabs
        links={[
          { name: 'Overview', route: '/' },
          { name: 'Patients', route: '/patients' },
          { name: 'Modules', route: '/modules' },
          { name: 'Forms', route: '/forms' },
        ]}
      />
      {children}
    </div>
  );
}
