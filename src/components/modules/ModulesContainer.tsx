import React, { ReactElement, useState, useEffect } from 'react';
import ModulesTable from '../tables/ModulesTable';
import TableActions from '../tables/TableActions';
import TableFilters from '../tables/TableFilters';
import ModuleIcon from '@material-ui/icons/ViewModuleOutlined';
import { Filter, ModuleRowProps } from '../tables/interfaces';
import { FilterChipProps } from '../common/interfaces';
import { modulesData } from '../../data/modules';
import _ from 'lodash';

export default function ModulesContainer(): ReactElement {
  const [filters, setFilters] = useState<Array<Filter>>([]);
  const [tableData, setTableData] = useState<Array<ModuleRowProps>>(modulesData);
  const [search, setSearch] = useState<string>('');
  const [preDefinedFilters, setPreDefinedFilters] = useState<Array<FilterChipProps>>([
    {
      color: '#F09E00',
      label: 'Modules Due Soon',
      icon: <ModuleIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Progress',
            field: 'progress',
          },
          value: {
            label: '> 50%',
            value: '50',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
    {
      color: '#D81B60',
      label: 'Modules Overdue',
      icon: <ModuleIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Due Date',
            field: 'dueDate',
          },
          value: {
            label: 'before Today',
            value: '< today',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
    {
      color: '#22B07D',
      label: 'Modules Recently Completed',
      icon: <ModuleIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Progress',
            field: 'progress',
          },
          value: {
            label: '= 100%',
            value: '100',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
  ]);

  useEffect(() => {
    const newData = modulesData;
    // for (const f of filters) {
    //    newData = newData.filter((elem) => elem.doctor.id === f.value.value);
    // }
    setTableData([...newData]);
  }, [filters, search]);

  useEffect(() => {
    for (let i = 0; i < preDefinedFilters.length; i++) {
      if (_.isEqual(filters, preDefinedFilters[i].filters)) {
        preDefinedFilters[i].selected = true;
      } else {
        preDefinedFilters[i].selected = false;
      }
    }
    setPreDefinedFilters([...preDefinedFilters]);
  }, [filters]);

  return (
    <>
      <TableActions
        preDefinedFilters={preDefinedFilters}
        primaryAction={{ label: 'Assign Module', action: '' }}
      />
      <TableFilters
        filters={filters}
        onSearch={(e) => {
          setSearch(e.target.value);
        }}
      />
      <ModulesTable data={tableData} />
    </>
  );
}
