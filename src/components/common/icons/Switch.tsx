import React, { ReactElement } from 'react';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

export default function SwitchIcon(props: SvgIconProps): ReactElement {
  return (
    <SvgIcon {...props}>
      <path
        d="M10.6667 17.7647L6.4 22L5.39022 20.9976L7.94311 18.4706H0V10H1.42222V17.0588H7.94311L5.39022 14.5318L6.4 13.5294L10.6667 17.7647Z"
        fill={props.color}
      />
      <path
        d="M5.33334 4.23529L9.6 -1.51318e-06L10.6098 1.00235L8.05689 3.52941L16 3.52941L16 12L14.5778 12L14.5778 4.94118L8.05689 4.94117L10.6098 7.46823L9.6 8.47059L5.33334 4.23529Z"
        fill={props.color}
      />
    </SvgIcon>
  );
}
