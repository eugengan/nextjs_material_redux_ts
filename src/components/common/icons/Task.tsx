import React, { ReactElement } from 'react';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

export default function TaskIcon(props: SvgIconProps): ReactElement {
  return (
    <SvgIcon {...props}>
      <mask
        id="taskIcon"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="4"
        y="2"
        width="16"
        height="20"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14 2H6C4.9 2 4.01 2.9 4.01 4L4 20C4 21.1 4.89 22 5.99 22H18C19.1 22 20 21.1 20 20V8L14 2ZM6 20V4H13V9H18V20H6ZM7.3 13.57L11.14 17.41L16.72 11.82L15.3 10.41L11.14 14.57L8.71 12.15L7.3 13.57Z"
          fill="white"
        />
      </mask>
      <g mask="url(#taskIcon)">
        <rect width="24" height="24" fill={props.color} />
      </g>
    </SvgIcon>
  );
}
