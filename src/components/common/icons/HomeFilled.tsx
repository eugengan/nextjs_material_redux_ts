import React, { ReactElement } from 'react';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

export default function HomeFilledIcon(props: SvgIconProps): ReactElement {
  return (
    <SvgIcon {...props}>
      <path d="M12 3L4 9V21H9V13H15V21H20V9L12 3Z" fill={props.color} />
      <mask
        id="emdHomeFilledIcon"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="4"
        y="3"
        width="16"
        height="18"
      >
        <path d="M12 3L4 9V21H9V13H15V21H20V9L12 3Z" fill="white" />
      </mask>
      <g mask="url(#emdHomeFilledIcon)">
        <rect width="24" height="24" fill={props.color} />
      </g>
    </SvgIcon>
  );
}
