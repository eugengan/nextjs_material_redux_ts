import React, { ReactElement } from 'react';
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';

export default function TreeIcon(props: SvgIconProps): ReactElement {
  return (
    <SvgIcon {...props}>
      <mask
        id="treeIcon"
        mask-type="alpha"
        maskUnits="userSpaceOnUse"
        x="3"
        y="5"
        width="18"
        height="15"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12 14.7C11.84 14.46 11.66 14.22 11.48 14C10.77 13.15 9.94 12.41 9.02 11.81C9.13 9.33 10.11 6.89 12 5C13.89 6.89 14.87 9.33 14.98 11.81C14.05 12.41 13.22 13.15 12.52 14C12.34 14.23 12.17 14.46 12 14.7ZM12.87 10.86C12.72 9.96 12.43 9.1 12 8.31C11.57 9.1 11.28 9.97 11.13 10.86C11.43 11.1 11.72 11.35 12 11.62C12.28 11.36 12.57 11.1 12.87 10.86ZM12 20C16.97 20 21 15.97 21 11C16.03 11 12 15.03 12 20ZM14.44 17.56C15.15 15.66 16.66 14.14 18.56 13.44C17.85 15.34 16.34 16.85 14.44 17.56ZM3 11C3 15.97 7.03 20 12 20C12 15.03 7.97 11 3 11ZM5.44 13.44C7.34 14.15 8.86 15.66 9.56 17.56C7.66 16.85 6.15 15.34 5.44 13.44Z"
          fill="white"
        />
      </mask>
      <g mask="url(#treeIcon)">
        <rect width="24" height="24" fill={props.color} />
      </g>
    </SvgIcon>
  );
}
