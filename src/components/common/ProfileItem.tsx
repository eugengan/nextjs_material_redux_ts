import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import AvatarPartners from './AvatarPartners';
import Typography from '@material-ui/core/Typography';
import { ProfileItemProps } from './interfaces';

const useStyles = makeStyles(
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
    },
    avatar: {
      marginTop: -2,
    },
    details: {
      marginLeft: 12,
    },
    caption: {
      marginTop: -5,
    },
  })
);

export default function ProfileItem(props: ProfileItemProps): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AvatarPartners
        className={classes.avatar}
        primary={{ src: props.avatar.src, initials: props.avatar.initials }}
      />
      <div className={classes.details}>
        <Typography variant="subtitle2">{props.name}</Typography>
        <Typography variant="caption" component="h6" className={classes.caption}>
          {props.id}
        </Typography>
      </div>
    </div>
  );
}
