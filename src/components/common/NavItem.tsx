import React, { ReactElement } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { NavItemProps } from './interfaces';
import Link from 'next/link';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const useStyles = makeStyles(({ spacing, palette, typography }: Theme) =>
  createStyles({
    root: {
      color: palette.primary.dark,
      fontWeight: typography.fontWeightMedium,
    },
    icon: {
      color: palette.primary.dark,
      minWidth: 'auto',
      marginRight: spacing(3),
      fontSize: typography.pxToRem(18),
    },
    text: {},
  })
);

export default function NavItem(props: NavItemProps): ReactElement {
  const classes = useStyles();
  return (
    <Link href={props.link}>
      <ListItem button className={classes.root}>
        <ListItemIcon className={classes.icon}>{props.icon}</ListItemIcon>
        <ListItemText className={classes.text}>{props.text}</ListItemText>
      </ListItem>
    </Link>
  );
}
