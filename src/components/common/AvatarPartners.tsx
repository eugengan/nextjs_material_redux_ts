import React, { ReactElement } from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { AvatarProps, AvatarPartnersProps } from './interfaces';

const useStyles = makeStyles(
  createStyles({
    root: {
      display: 'flex',
    },
    primaryAvatar: {},
    secondaryAvatar: {
      width: 32,
      height: 32,
      fontSize: 10,
      marginLeft: -6,
      marginTop: 16,
    },
  })
);

const renderAvatar = (props: AvatarProps, className: string) => (
  <Avatar className={className} src={props.src}>
    {props.initials}
  </Avatar>
);

export default function NavItem(props: AvatarPartnersProps): ReactElement {
  const classes = useStyles();
  return (
    <div className={clsx(classes.root, props.className)}>
      {renderAvatar(props.primary, classes.primaryAvatar)}
      {props.secondary ? renderAvatar(props.secondary, classes.secondaryAvatar) : null}
    </div>
  );
}
