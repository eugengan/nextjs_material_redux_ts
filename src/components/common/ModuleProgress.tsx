import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { AvatarProps, ModuleProgressProps } from './interfaces';
import Avatar from '@material-ui/core/Avatar';
import LinearProgress from '@material-ui/core/LinearProgress';
import clsx from 'clsx';

const MAX_WIDTH = 180;
const STEP = MAX_WIDTH / 100;
const AVATAR_SIZE = 32;
const OFFSET = AVATAR_SIZE / 2;

const calculateOffset = (
  primaryProgress: number,
  secondaryProgress: number,
  type: 'primary' | 'secondary'
) => {
  if (!secondaryProgress) return 0;
  if (primaryProgress === secondaryProgress) return 0;
  if (primaryProgress > secondaryProgress && type === 'primary') return 0;
  if (primaryProgress < secondaryProgress && type === 'secondary') return 0;
  if (primaryProgress > secondaryProgress && type === 'secondary') return OFFSET;
  if (primaryProgress < secondaryProgress && type === 'primary') return OFFSET;
  return 0;
};

const useStyles = makeStyles(
  createStyles({
    root: {
      width: MAX_WIDTH,
      margin: '0 auto',
    },
    avatar: {
      width: AVATAR_SIZE,
      height: AVATAR_SIZE,
      fontSize: 10,
      marginRight: 2,
      marginBottom: 4,
    },
    avatarTrack: {
      display: 'flex',
    },
    avatarPrimary: ({ primary, secondary }: ModuleProgressProps) => {
      return {
        left:
          primary.progress * STEP -
          calculateOffset(primary.progress, secondary ? secondary.progress : 0, 'primary'),
      };
    },
    avatarSecondary: ({ primary, secondary }: ModuleProgressProps) => {
      const offset = secondary
        ? secondary.progress * STEP -
          calculateOffset(primary.progress, secondary.progress, 'secondary')
        : 0;
      return {
        left: offset,
      };
    },
    progressPrimary: {},
    progressSecondary: {
      backgroundColor: 'transparent',
      top: -12,
      opacity: 0.5,
    },
  })
);

const renderAvatar = (props: AvatarProps, className: string) => (
  <Avatar className={className} src={props.src}>
    {props.initials}
  </Avatar>
);

const renderProgress = (progress: number, className: string) => (
  <LinearProgress className={className} variant="determinate" value={progress} />
);

export default function ModuleProgress(props: ModuleProgressProps): ReactElement {
  const classes = useStyles(props);
  const { primary, secondary } = props;

  return (
    <div className={classes.root}>
      <div className={classes.avatarTrack}>
        {renderAvatar(primary, clsx(classes.avatar, classes.avatarPrimary))}
        {secondary ? renderAvatar(secondary, clsx(classes.avatar, classes.avatarSecondary)) : null}
      </div>
      {renderProgress(primary.progress, classes.progressPrimary)}
      {secondary ? renderProgress(secondary.progress, classes.progressSecondary) : null}
    </div>
  );
}
