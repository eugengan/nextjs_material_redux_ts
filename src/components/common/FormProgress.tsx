import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { FormProgressProps } from './interfaces';
import clsx from 'clsx';

const useStyles = makeStyles(({ palette }: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatar: {
      margin: '0 2px',
    },
    inActive: {
      width: 32,
      height: 32,
      opacity: 0.4,
    },
    active: {
      width: 40,
      height: 40,
      border: `2px solid ${palette.primary.light}`,
    },
  })
);

export default function FormProgress(props: FormProgressProps): ReactElement {
  const classes = useStyles();
  const { users, active } = props;

  const renderAvatars = () =>
    users.map((elem, i) => (
      <Avatar
        src={elem.imgSrc}
        key={i}
        className={clsx(classes.avatar, elem.id === active ? classes.active : classes.inActive)}
      >
        {elem.initials}
      </Avatar>
    ));

  return <div className={classes.root}>{renderAvatars()}</div>;
}
