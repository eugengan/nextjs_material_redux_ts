import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import { FilterChipProps } from './interfaces';

const useStyles = makeStyles(() =>
  createStyles({
    root: (props: FilterChipProps) => {
      const { selected, color } = props;
      return {
        backgroundColor: selected ? color : 'transparent',
        '& .MuiChip-icon': {
          color: selected ? color : '#fff',
          backgroundColor: selected ? '#fff' : color,
        },
        '&:hover': {
          backgroundColor: selected ? color : 'transparent',
        },
        '&:active': {
          backgroundColor: selected ? color : 'transparent',
        },
        '&:focus': {
          backgroundColor: selected ? color : 'transparent',
        },
      };
    },
  })
);

export default function FilterChip(props: FilterChipProps): ReactElement {
  const classes = useStyles(props);
  return (
    <Chip
      variant={props.selected ? 'default' : 'outlined'}
      className={classes.root}
      icon={props.icon}
      label={props.label}
      onClick={() => {
        if (!props.selected) props.onClick(props.filters);
      }}
    />
  );
}
