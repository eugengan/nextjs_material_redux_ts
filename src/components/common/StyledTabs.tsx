import React, { ReactElement } from 'react';
import { makeStyles, withStyles, Theme, createStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { useRouter } from 'next/router';

interface StyledTabsProps {
  value: string;
  onChange: (event: React.ChangeEvent<Record<string, unknown>>, newValue: string) => void;
}

const StyledTabs = withStyles(({ palette }: Theme) =>
  createStyles({
    indicator: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      height: 4,
      '& > span': {
        width: '100%',
        borderRadius: '100px 100px 0 0',
        backgroundColor: fade(palette.primary.main, 0.8),
      },
    },
  })
)((props: StyledTabsProps) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

interface StyledTabProps {
  label: string;
  value: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const StyledTab = withStyles((theme: Theme) =>
  createStyles({
    root: {
      textTransform: 'none',
      color: theme.palette.primary.dark,
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(17),
      marginRight: theme.spacing(6),
      padding: '16px 2px 20px',
      letterSpacing: 0.1,
      opacity: 1,
      minWidth: 0,
      '&.Mui-selected': {
        color: theme.palette.primary.main,
      },
      '&:focus': {
        color: theme.palette.primary.main,
      },
    },
  })
)((props: StyledTabProps) => <Tab disableRipple {...props} />);

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    borderBottom: '1px solid #E6EBEE',
  },
  padding: {
    padding: theme.spacing(3),
  },
}));

interface TabLink {
  name: string;
  route: string;
}

type TabProps = { links: Array<TabLink> };

export default function CustomizedTabs(props: TabProps): ReactElement {
  const classes = useStyles();
  const route = useRouter();
  const [value, setValue] = React.useState(route.pathname);

  const handleChange = (_event: React.ChangeEvent<Record<string, unknown>>, newValue: string) => {
    setValue(newValue);
  };

  const renderTabs = (list: Array<TabLink>) => {
    return list.map((elem, i) => (
      <StyledTab
        label={elem.name}
        value={elem.route}
        key={i}
        onClick={() => route.push(elem.route)}
      />
    ));
  };

  return (
    <div className={classes.root}>
      <StyledTabs value={value} onChange={handleChange} aria-label="styled tabs example">
        {renderTabs(props.links)}
      </StyledTabs>
    </div>
  );
}
