import { IconProps } from '@material-ui/core';
import { Filter, User } from '../tables/interfaces';

export interface NavItemProps {
  text: string;
  icon: IconProps;
  link: string;
  active?: boolean;
}

export interface AvatarProps {
  src?: string;
  initials: string;
}

export interface AvatarPartnersProps {
  primary: AvatarProps;
  secondary?: AvatarProps | null;
  className?: string;
}

export interface ProfileItemProps {
  avatar: AvatarProps;
  name: string;
  id: string;
}

export interface FilterChipProps {
  icon: React.ReactElement;
  label: string;
  color?: string;
  selected?: boolean;
  filters: Array<Filter>;
  onClick: (filters: Array<Filter>) => void;
}

export interface AvatarProgressProps extends AvatarProps {
  progress: number;
}

export interface ModuleProgressProps {
  primary: AvatarProgressProps;
  secondary?: AvatarProgressProps | null;
}

export interface FormProgressProps {
  users: Array<User>;
  active: string;
}
