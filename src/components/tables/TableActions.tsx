import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import FilterChip from '../common/FilterChip';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { TableActionsProps } from './interfaces';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {
      padding: `${spacing(2)}px 0`,
      display: 'flex',
      alignItems: 'center',
      '& .MuiButton-root': {
        marginRight: spacing(1),
        '&:last-of-type': {
          marginRight: 0,
        },
      },
    },
    actionsRight: {
      marginLeft: 'auto',
    },
  })
);

export default function TableActions(props: TableActionsProps): ReactElement {
  const classes = useStyles();

  const renderPreDefinedFilterChips = () =>
    props.preDefinedFilters.map((elem, i) => <FilterChip {...elem} key={i} />);

  return (
    <div className={classes.root}>
      {renderPreDefinedFilterChips()}
      <div className={classes.actionsRight}>
        <Button variant="outlined">Export CSV</Button>
        <Button variant="outlined" startIcon={<AddIcon />}>
          {props.primaryAction.label}
        </Button>
      </div>
    </div>
  );
}
