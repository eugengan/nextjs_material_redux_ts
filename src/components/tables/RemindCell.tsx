import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import dayjs from 'dayjs';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import { RemindCellProps } from './interfaces';

dayjs.extend(LocalizedFormat);

const useStyles = makeStyles(({ spacing, palette, typography }: Theme) =>
  createStyles({
    date: {
      color: palette.text.disabled,
      fontSize: typography.pxToRem(12),
      marginTop: spacing(1),
    },
  })
);

export default function RemindCell(props: RemindCellProps): ReactElement {
  const classes = useStyles();
  const remindedDate = dayjs(props.date).format('ll');
  return (
    <>
      <Button variant="outlined">Remind</Button>
      <Typography variant="body2" className={classes.date}>
        {remindedDate}
      </Typography>
    </>
  );
}
