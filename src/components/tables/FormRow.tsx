import React, { ReactElement } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { FormRowProps } from './interfaces';
import RemindCell from './RemindCell';
import NameCell from './NameCell';
import DateCell from './DateCell';
import FormProgress from '../common/FormProgress';

export default function FormRow(props: FormRowProps): ReactElement {
  return (
    <TableRow>
      <TableCell>
        <NameCell name={props.name} users={props.signers} />
      </TableCell>
      <TableCell align="center">
        <FormProgress users={props.signers} active={props.activeSigner} />
      </TableCell>
      <TableCell align="center">
        <DateCell date={props.dueDate} />
      </TableCell>
      <TableCell align="right">
        <RemindCell date={props.remindedDate} />
      </TableCell>
    </TableRow>
  );
}
