import React, { ReactElement } from 'react';
import BaseTable from './BaseTable';
import { FormRowProps, FormsTableProps } from './interfaces';
import FormRow from './FormRow';

export default function FormsTable(props: FormsTableProps): ReactElement {
  const rows = props.data.map((elem: FormRowProps, i: number) => <FormRow {...elem} key={i} />);
  return (
    <BaseTable
      columns={[
        { title: 'Form' },
        { title: 'Status', align: 'center' },
        { title: 'Due Date', align: 'center' },
        { title: '' },
      ]}
      rows={rows}
    />
  );
}
