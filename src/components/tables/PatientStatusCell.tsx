import React, { ReactElement, useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import theme from '../../theme';
import Typography from '@material-ui/core/Typography';
import StatusIcon from '@material-ui/icons/FiberManualRecord';
import Link from '@material-ui/core/Link';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { PatientStatus, PatientStatusCellProps } from './interfaces';

dayjs.extend(relativeTime);

const useStyles = makeStyles(
  createStyles({
    statusCell: {
      '& .MuiTypography-root': {
        marginBottom: 1,
        '&.noIcon': {
          paddingLeft: 14,
        },
        '&:last-of-type': {
          marginBottom: 0,
        },
      },
    },
    statusIcon: {
      fontSize: 10,
    },
  })
);

export default function PatientStatusCell(props: PatientStatusCellProps): ReactElement {
  const classes = useStyles();

  const statusToColor = (status: 'incomplete' | 'overdue' | 'complete') => {
    let color: string;
    switch (status) {
      case 'incomplete':
        color = theme.palette.warning.main;
        break;
      case 'overdue':
        color = theme.palette.error.main;
        break;
      default:
        color = theme.palette.primary.main;
        break;
    }
    return color;
  };

  const [show, setShow] = useState(false);

  const renderStatus = (statusItem: PatientStatus, key: number) => {
    const statusColor = statusToColor(statusItem.status);
    return (
      <Typography variant="body2" key={key}>
        <StatusIcon className={classes.statusIcon} style={{ color: statusColor }} />{' '}
        {statusItem.count} {statusItem.type}s {statusItem.status}
      </Typography>
    );
  };

  const renderStatusList = (list: Array<PatientStatus>) => {
    let showList: Array<PatientStatus>;
    show ? (showList = list) : (showList = list.slice(0, 2));
    return showList.map((item, i) => renderStatus(item, i));
  };

  const renderMoreLink = () => (
    <Typography variant="body2" className={'noIcon'}>
      <Link href="#" onClick={() => setShow(true)} hidden={show}>
        +{props.items.length - 2} more
      </Link>
    </Typography>
  );

  return (
    <div className={classes.statusCell}>
      {renderStatusList(props.items)}
      {props.items.length > 2 ? renderMoreLink() : null}
      <Typography variant="caption" color="textSecondary" className={'noIcon'}>
        {dayjs().to(props.timeStamp)}
      </Typography>
    </div>
  );
}
