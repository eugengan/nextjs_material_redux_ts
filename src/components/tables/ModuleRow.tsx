import React, { ReactElement } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { ModuleRowProps } from './interfaces';

import ModuleProgress from '../common/ModuleProgress';
import RemindCell from './RemindCell';
import DateCell from './DateCell';
import NameCell from './NameCell';

export default function ModuleRow(props: ModuleRowProps): ReactElement {
  return (
    <TableRow>
      <TableCell>
        <NameCell name={props.name} users={props.patients} />
      </TableCell>
      <TableCell align="center">
        <ModuleProgress
          primary={{ src: '/images/test-avatar-patient-01.png', initials: 'BM', progress: 30 }}
          secondary={{ src: '', initials: 'AM', progress: 0 }}
        />
      </TableCell>
      <TableCell align="center">
        <DateCell date={props.dueDate} />
      </TableCell>
      <TableCell align="right">
        <RemindCell date={props.remindedDate} />
      </TableCell>
    </TableRow>
  );
}
