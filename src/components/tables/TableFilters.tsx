import React, { ReactElement, useState } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { Filter, TableFiltersProps } from './interfaces';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import DeleteIcon from '@material-ui/icons/Clear';
import Chip, { ChipProps } from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {
      marginBottom: spacing(2),
      '& .MuiButton-root': {
        marginRight: spacing(1),
      },
      '& .MuiChip-root': {
        marginRight: spacing(1),
      },
    },
    searchButton: {
      height: 32,
      borderRadius: 4,
    },
    searchInput: {
      marginTop: -1,
      marginRight: 8,
    },
  })
);

export default function TableFitlers(props: TableFiltersProps): ReactElement {
  const classes = useStyles();
  const [searchOpen, setSearchOpen] = useState(false);

  const renderFilter = (props: ChipProps, key: number) => (
    <Chip {...props} key={key} size="small" color="primary" deleteIcon={<DeleteIcon />} />
  );

  const renderFilters = () =>
    props.filters.map((elem: Filter, i: number) =>
      renderFilter(
        {
          label: (
            <>
              <strong>{elem.field.label}</strong> {elem.value.label}
            </>
          ),
          onDelete: () => {
            const index = elem.filtersList.findIndex(
              (obj) => obj.field.field === elem.field.field && obj.value.value === elem.value.value
            );
            elem.setState(elem.filtersList.slice(0, index));
          },
        },
        i
      )
    );

  const renderSearchButton = () => (
    <IconButton
      aria-label="search"
      className={classes.searchButton}
      hidden={searchOpen}
      onClick={() => setSearchOpen(true)}
    >
      <SearchIcon />
    </IconButton>
  );

  const renderSearchInput = () => (
    <TextField
      hidden={!searchOpen}
      className={classes.searchInput}
      variant="outlined"
      placeholder="Search Keyword"
      margin="dense"
      InputProps={{
        startAdornment: (
          <>
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          </>
        ),
      }}
      onChange={props.onSearch}
    />
  );

  return (
    <div className={classes.root}>
      {searchOpen ? renderSearchInput() : renderSearchButton()}
      {renderFilters()}
      <Button size="small" color="primary" startIcon={<AddIcon />}>
        Add Filter
      </Button>
      <Button size="small">Hide filters</Button>
    </div>
  );
}
