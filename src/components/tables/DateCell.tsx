import React, { ReactElement } from 'react';
import Typography from '@material-ui/core/Typography';
import dayjs from 'dayjs';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import { DateCellProps } from './interfaces';

dayjs.extend(LocalizedFormat);

export default function RemindCell(props: DateCellProps): ReactElement {
  const date = dayjs(props.date).format('ll');
  return (
    <>
      <Typography variant="body2">{date}</Typography>
    </>
  );
}
