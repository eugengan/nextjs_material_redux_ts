import React, { ReactElement } from 'react';

import BaseTable from './BaseTable';
import { ModuleRowProps, ModulesTableProps } from './interfaces';
import ModuleRow from './ModuleRow';

export default function ModulesTable(props: ModulesTableProps): ReactElement {
  const rows = props.data.map((elem: ModuleRowProps, i: number) => <ModuleRow {...elem} key={i} />);
  return (
    <BaseTable
      columns={[
        { title: 'Module' },
        { title: 'Status', align: 'center' },
        { title: 'Due Date', align: 'center' },
        { title: '' },
      ]}
      rows={rows}
    />
  );
}
