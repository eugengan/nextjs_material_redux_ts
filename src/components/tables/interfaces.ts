import { TableCellProps } from '@material-ui/core';
import { ReactElement } from 'react';
import { FilterChipProps } from '../common/interfaces';

export interface BaseTableHeader {
  title: string;
  align?: TableCellProps['align'];
}

export interface BaseTableProps {
  columns: Array<BaseTableHeader>;
  rows: Array<ReactElement>;
}
export interface PatientRowProps {
  primary: User;
  secondary?: User;
  doctor: User;
  status: PatientStatusCellProps;
}

export interface PatientsTableProps {
  data: Array<PatientRowProps>;
}

export interface ModulesTableProps {
  data: Array<ModuleRowProps>;
}

export interface FormsTableProps {
  data: Array<FormRowProps>;
}

export interface User {
  imgSrc?: string;
  name: string;
  initials: string;
  id: string;
}

export interface PatientStatus {
  count: number;
  type: 'module' | 'form';
  status: 'complete' | 'incomplete' | 'overdue';
}

export interface PatientStatusCellProps {
  items: Array<PatientStatus>;
  timeStamp: Date;
}

export interface RemindCellProps {
  date: Date;
}

export interface DateCellProps {
  date: Date;
}

export interface NameCellProps {
  name: string;
  users?: Array<User>;
}

export interface ModuleRowProps {
  id: number;
  name: string;
  patients: Array<User>;
  progress: number;
  dueDate: Date;
  remindedDate: Date;
}

export interface FormRowProps {
  id: number;
  name: string;
  signers: Array<User>;
  activeSigner: string;
  dueDate: Date;
  remindedDate: Date;
}

export interface TableActionsProps {
  preDefinedFilters: Array<FilterChipProps>;
  primaryAction: {
    label: string;
    action: string;
  };
}

export interface Filter {
  field: {
    label: string;
    field: string;
  };
  value: {
    label: string;
    value: string;
  };
  filtersList: Array<Filter>;
  setState: (state: Array<Filter>) => void;
}

export interface TableFiltersProps {
  filters: Array<Filter>;
  onSearch: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
