import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { BaseTableProps } from './interfaces';

const useStyles = makeStyles(
  createStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 'calc(100vh - 300px)',
    },
    tableBody: {
      '& .MuiTableRow-root:last-of-type .MuiTableCell-root': {
        borderBottom: 'none',
      },
    },
  })
);

export default function BaseTable(props: BaseTableProps): ReactElement {
  const classes = useStyles();
  const renderHeaders = () =>
    props.columns.map((elem, i) => (
      <TableCell align={elem.align ? elem.align : 'inherit'} key={i}>
        {elem.title}
      </TableCell>
    ));

  return (
    <>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>{renderHeaders()}</TableRow>
          </TableHead>
          <TableBody className={classes.tableBody}>{props.rows}</TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
