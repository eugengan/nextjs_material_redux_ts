import React, { ReactElement } from 'react';

import BaseTable from './BaseTable';
import { PatientRowProps, PatientsTableProps } from './interfaces';
import PatientRow from './PatientRow';

export default function PatientsTable(props: PatientsTableProps): ReactElement {
  const patientRows = props.data.map((elem: PatientRowProps, i) => (
    <PatientRow key={i} {...elem} />
  ));
  return (
    <BaseTable
      columns={[{ title: 'Patient' }, { title: 'ID' }, { title: 'Doctor' }, { title: 'Recent' }]}
      rows={patientRows}
    />
  );
}
