import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

import AvatarPartners from '../common/AvatarPartners';
import PatientStatusCell from './PatientStatusCell';
import { PatientRowProps } from './interfaces';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    patientCell: {
      display: 'flex',
      flexDirection: 'row',
    },
    avatar: {
      minWidth: 74,
    },
    patientNames: {
      marginLeft: spacing(2),
    },
    link: {
      textDecoration: 'none',
      color: 'inherit',
    },
  })
);

export default function PatientRow(props: PatientRowProps): ReactElement {
  const classes = useStyles();
  const { primary, secondary, doctor, status } = props;
  return (
    <TableRow>
      <TableCell>
        <div className={classes.patientCell}>
          <AvatarPartners
            className={classes.avatar}
            primary={{ src: primary.imgSrc, initials: primary.initials }}
            secondary={secondary ? { src: secondary.imgSrc, initials: secondary.initials } : null}
          />
          <div className={classes.patientNames}>
            <Typography variant="body2">
              <Link href="/patient" color="inherit">
                {primary.name}
              </Link>
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {secondary ? secondary.name : ''}
            </Typography>
          </div>
        </div>
      </TableCell>
      <TableCell>
        <Typography variant="body2">{primary.id}</Typography>
        <Typography variant="body2" color="textSecondary">
          {secondary ? secondary.id : ''}
        </Typography>
      </TableCell>
      <TableCell>
        <Typography variant="body2">{doctor.name}</Typography>
      </TableCell>
      <TableCell>
        <PatientStatusCell items={status.items} timeStamp={status.timeStamp} />
      </TableCell>
    </TableRow>
  );
}
