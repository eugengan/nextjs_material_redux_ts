import React, { ReactElement } from 'react';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { NameCellProps, User } from './interfaces';

export default function NameCell(props: NameCellProps): ReactElement {
  const renderUsers = (users: Array<User>) => (
    <Typography variant="body2" color="textSecondary">
      {renderUserList(users)}
    </Typography>
  );

  const renderUserList = (users: Array<User>) => {
    const length = users.length;
    return users.map((elem, i) => (
      <React.Fragment key={i}>
        <Link href={'#' + elem.id} color="textSecondary">
          {elem.name}
        </Link>
        {length > i + 1 ? ', ' : ''}
      </React.Fragment>
    ));
  };

  return (
    <>
      <Typography variant="body2">
        <Link href="#" color="textPrimary">
          {props.name}
        </Link>
      </Typography>
      {props.users ? renderUsers(props.users) : null}
    </>
  );
}
