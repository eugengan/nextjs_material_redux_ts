import React, { ReactElement } from 'react';
import List from '@material-ui/core/List';
import Widget from './Widget';
import WidgetListItem from './WidgetListItem';

export default function FormsWidget(): ReactElement {
  return (
    <Widget title="Modules">
      <List component="nav" aria-label="Modules Overview List">
        <WidgetListItem text="There are 5 Modules overdue" status="error" type="module" />
        <WidgetListItem text="There are 5 Modules due soon" status="warning" type="module" />
      </List>
    </Widget>
  );
}
