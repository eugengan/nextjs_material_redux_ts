import React, { ReactElement } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';

import UserIcon from '@material-ui/icons/PersonOutline';
import TaskIcon from '../common/icons/Task';
import TreeIcon from '../common/icons/Tree';
import ClockIcon from '@material-ui/icons/WatchLaterOutlined';

import { ImpactProps } from './interfaces';
import ImpactListItem from './ImpactListItem';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {},
    heading: {
      marginBottom: spacing(2),
    },
  })
);

export default function Impact(props: ImpactProps): ReactElement {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, props.className)}>
      <Typography variant="subtitle1" className={classes.heading}>
        Your Impact
      </Typography>
      <Grid container spacing={5}>
        <Grid item md={3}>
          <ImpactListItem icon={<UserIcon />} primaryText="Patients" secondaryText="134" />
        </Grid>
        <Grid item md={3}>
          <ImpactListItem icon={<TaskIcon />} primaryText="Forms Completed" secondaryText="1,234" />
        </Grid>
        <Grid item md={3}>
          <ImpactListItem icon={<TreeIcon />} primaryText="Trees Saved" secondaryText="2,789" />
        </Grid>
        <Grid item md={3}>
          <ImpactListItem icon={<ClockIcon />} primaryText="Time Saved" secondaryText="52d 6h" />
        </Grid>
      </Grid>
    </div>
  );
}
