import { IconProps } from '@material-ui/core';

export interface WidgetProps {
  title: string;
  children: React.ReactNode;
}

export interface WidgetListProps {
  text: string;
  status: 'error' | 'warning' | 'info';
  type: 'module' | 'form';
}

export interface ImpactProps {
  className?: string;
}

export interface ImpactListProps {
  primaryText: string;
  secondaryText: string;
  icon: IconProps;
}
