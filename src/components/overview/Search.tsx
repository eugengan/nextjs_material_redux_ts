import React, { ReactElement } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Autocomplete from '@material-ui/lab/Autocomplete';

import SearchIcon from '@material-ui/icons/Search';

interface PatientSearchItem {
  name: string;
  image: string;
}

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    input: {
      marginRight: spacing(2),
    },
  })
);

export default function Search(): ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Autocomplete
        id="overview-search"
        freeSolo
        options={patientsList.map((option) => option.name)}
        fullWidth
        className={classes.input}
        renderInput={(params) => (
          <TextField
            {...params}
            id="overview-search-input-with-icon-textfield"
            variant="outlined"
            placeholder="Search patients"
            InputProps={{
              ...params.InputProps,
              startAdornment: (
                <>
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                </>
              ),
            }}
          />
        )}
      />
      <Button variant="contained" color="primary" size="large">
        Search
      </Button>
    </div>
  );
}

const patientsList: Array<PatientSearchItem> = [
  { name: 'McClinctok, Barbara', image: '/images/test-avatar-patient-01' },
  { name: 'McClinctok, Barbara', image: '/images/test-avatar-patient-02' },
];
