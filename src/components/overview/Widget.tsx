import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { WidgetProps } from './interfaces';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: '#F2F5F7',
      borderRadius: 8,
      padding: theme.spacing(3),
      minHeight: 270,
    },
    tite: {
      marginBottom: theme.spacing(2),
    },
    content: {},
    emptyStateImg: {
      textAlign: 'center',
    },
    emptyStateCaption: {
      color: theme.palette.text.disabled,
      marginTop: theme.spacing(2),
    },
  })
);

export default function Widget(props: WidgetProps): ReactElement {
  const classes = useStyles();

  const renderContent = () => {
    if (props.children) {
      return <div className={classes.content}>{props.children}</div>;
    } else {
      return (
        <>
          <div className={classes.emptyStateImg}>
            <img src="/images/caught-up.svg" alt="Caught up!" />
          </div>
          <Typography variant="body2" className={classes.emptyStateCaption} align="center">
            Forms are caught up
          </Typography>
        </>
      );
    }
  };

  return (
    <div className={classes.root}>
      <Typography variant="subtitle1">{props.title}</Typography>
      {renderContent()}
    </div>
  );
}
