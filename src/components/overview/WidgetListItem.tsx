import React, { ReactElement } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import ModuleIcon from '@material-ui/icons/Tv';
import FormIcon from '@material-ui/icons/Create';
import ArrowIcon from '@material-ui/icons/KeyboardArrowRight';
import { WidgetListProps } from './interfaces';

const returnPalette = (status: WidgetListProps['status'], palette: Theme['palette']) => {
  switch (status) {
    case 'error':
      return palette.error.main;
    case 'warning':
      return palette.warning.main;
    default:
      return palette.info.main;
  }
};

const useStyles = makeStyles(({ typography, palette, spacing }: Theme) =>
  createStyles({
    root: {},
    icon: (props: WidgetListProps) => {
      return {
        color: palette.common.white,
        minWidth: 24,
        minHeight: 24,
        marginRight: spacing(2),
        backgroundColor: returnPalette(props.status, palette),
        borderRadius: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 16,
      };
    },
    link: {
      fontWeight: typography.fontWeightBold,
      color: palette.primary.light,
    },
    linkIcon: {
      verticalAlign: 'bottom',
      fontSize: typography.pxToRem(18),
    },
  })
);

const renderIcon = (type: WidgetListProps['type']) =>
  type === 'form' ? <FormIcon fontSize="inherit" /> : <ModuleIcon fontSize="inherit" />;

export default function WidgetListItem(props: WidgetListProps): ReactElement {
  const classes = useStyles(props);

  return (
    <ListItem className={classes.root} disableGutters alignItems="flex-start">
      <ListItemIcon className={classes.icon}>{renderIcon(props.type)}</ListItemIcon>
      <ListItemText
        primary={<Typography variant="body2">{props.text}</Typography>}
        secondary={
          <Link href="#" className={classes.link}>
            {props.type === 'module' ? 'View' : 'Sign now'}
            <ArrowIcon className={classes.linkIcon} />
          </Link>
        }
      />
    </ListItem>
  );
}
