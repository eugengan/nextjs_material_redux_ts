import React, { ReactElement } from 'react';
import List from '@material-ui/core/List';
import Widget from './Widget';
import WidgetListItem from './WidgetListItem';

export default function FormsWidget(): ReactElement {
  return (
    <Widget title="Forms">
      <List component="nav" aria-label="Forms Overview List">
        <WidgetListItem
          text="You have 16 forms waiting for you to sign"
          status="info"
          type="form"
        />
      </List>
    </Widget>
  );
}
