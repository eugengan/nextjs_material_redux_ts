import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Search from '../overview/Search';
import FormsWidget from '../overview/FormsWidget';
import ModulesWidget from '../overview/ModulesWidget';
import Impact from '../overview/Impact';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 680,
      margin: '0 auto',
      maxWidth: '100%',
      paddingBottom: theme.spacing(10),
    },
    heading: {
      marginTop: theme.spacing(11),
      marginLeft: theme.spacing(1),
      marginBottom: theme.spacing(2),
    },
    widgets: {
      marginTop: theme.spacing(7),
    },
    impact: {
      marginTop: theme.spacing(12),
    },
  })
);

export default function OverviewContainer(): ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography className={classes.heading} variant="h5">
        Good afternoon, Charlie
      </Typography>
      <Search />

      <div className={classes.widgets}>
        <Grid container spacing={5}>
          <Grid item sm={6}>
            <FormsWidget />
          </Grid>
          <Grid item sm={6}>
            <ModulesWidget />
          </Grid>
        </Grid>
      </div>

      <Impact className={classes.impact} />
    </div>
  );
}
