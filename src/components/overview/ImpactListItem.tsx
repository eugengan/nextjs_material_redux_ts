import React, { ReactElement } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { ImpactListProps } from './interfaces';

const useStyles = makeStyles(({ spacing, palette, typography }: Theme) =>
  createStyles({
    root: {},
    icon: {
      marginBottom: spacing(0.5),
      fontSize: typography.pxToRem(24),
      color: palette.text.disabled,
    },
  })
);

export default function ImpactListItem(props: ImpactListProps): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.icon}>{props.icon}</div>
      <Typography component="h6" variant="caption" color="textSecondary">
        {props.primaryText}
      </Typography>
      <Typography variant="h5">{props.secondaryText}</Typography>
    </div>
  );
}
