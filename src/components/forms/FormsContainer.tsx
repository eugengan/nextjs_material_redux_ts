import React, { ReactElement, useState, useEffect } from 'react';
import FormsTable from '../tables/FormsTable';
import TableActions from '../tables/TableActions';
import TableFilters from '../tables/TableFilters';
import FormIcon from '@material-ui/icons/DescriptionOutlined';
import { Filter, FormRowProps } from '../tables/interfaces';
import { FilterChipProps } from '../common/interfaces';
import { formsData } from '../../data/forms';
import _ from 'lodash';

export default function FormsContainer(): ReactElement {
  const [filters, setFilters] = useState<Array<Filter>>([]);
  const [tableData, setTableData] = useState<Array<FormRowProps>>(formsData);
  const [search, setSearch] = useState<string>('');
  const [preDefinedFilters, setPreDefinedFilters] = useState<Array<FilterChipProps>>([
    {
      color: '#5F7AC0',
      label: 'My Forms to Sign',
      icon: <FormIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Current Signer',
            field: 'activeSigner',
          },
          value: {
            label: 'Me',
            value: '2422348',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
    {
      color: '#D81B60',
      label: 'Patient Forms Overdue',
      icon: <FormIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Due Date',
            field: 'dueDate',
          },
          value: {
            label: 'before Today',
            value: '< today',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
    {
      color: '#22B07D',
      label: 'Forms Completed Recently',
      icon: <FormIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Progress',
            field: 'progress',
          },
          value: {
            label: '= 100%',
            value: '100',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
  ]);

  useEffect(() => {
    const newData = formsData;
    // for (const f of filters) {
    //    newData = newData.filter((elem) => elem.doctor.id === f.value.value);
    // }
    setTableData([...newData]);
  }, [filters, search]);

  useEffect(() => {
    for (let i = 0; i < preDefinedFilters.length; i++) {
      if (_.isEqual(filters, preDefinedFilters[i].filters)) {
        preDefinedFilters[i].selected = true;
      } else {
        preDefinedFilters[i].selected = false;
      }
    }
    setPreDefinedFilters([...preDefinedFilters]);
  }, [filters]);
  return (
    <>
      <TableActions
        preDefinedFilters={preDefinedFilters}
        primaryAction={{ label: 'Assign Form', action: '' }}
      />
      <TableFilters
        filters={filters}
        onSearch={(e) => {
          setSearch(e.target.value);
        }}
      />
      <FormsTable data={tableData} />
    </>
  );
}
