import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import ModulesTable from '../tables/ModulesTable';
import FormsTable from '../tables/FormsTable';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {
      paddingTop: spacing(3),
      paddingBottom: spacing(3),
    },
    paper: {
      marginBottom: spacing(2),
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: `${spacing(1)}px 0`,
    },
    headerText: {
      marginLeft: spacing(1),
    },
    patientHeader: {
      display: 'flex',
    },
  })
);

export default function PatientView(): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <div className={classes.header}>
          <Typography variant="subtitle1" className={classes.headerText}>
            Modules
          </Typography>
          <Button variant="outlined" startIcon={<AddIcon />}>
            Assign Module
          </Button>
        </div>
        <ModulesTable
          data={[
            {
              id: 12312451,
              name: 'Intro to Fertility Treatment',
              patients: [
                {
                  id: '1231241',
                  name: 'Barbara McClintock',
                  initials: 'BM',
                  imgSrc: '/images/test-avatar-patient-01.png',
                },
                { id: '2412341', name: 'Abdul McClintock', initials: 'AM' },
              ],
              progress: 50,
              dueDate: new Date('October 31, 2020'),
              remindedDate: new Date('October 1, 2020'),
            },
          ]}
        />
      </Paper>
      <Paper className={classes.paper}>
        <div className={classes.header}>
          <Typography variant="subtitle1" className={classes.headerText}>
            Forms
          </Typography>
          <Button variant="outlined" startIcon={<AddIcon />}>
            Assign Form
          </Button>
        </div>
        <FormsTable
          data={[
            {
              id: 12312451,
              name: 'Intro to Fertility Treatment',
              signers: [
                {
                  id: '1231241',
                  name: 'Barbara McClintock',
                  initials: 'BM',
                  imgSrc: '/images/test-avatar-patient-01.png',
                },
                { id: '2412341', name: 'Abdul McClintock', initials: 'AM' },
                { id: '2422348', name: 'Cole Coleman', initials: 'CC' },
                { id: '2416341', name: 'Blair Doe', initials: 'AM' },
              ],
              activeSigner: '2422348',
              dueDate: new Date('October 31, 2020'),
              remindedDate: new Date('October 1, 2020'),
            },
          ]}
        />
      </Paper>
    </div>
  );
}
