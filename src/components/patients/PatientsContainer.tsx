import React, { ReactElement, useEffect, useState } from 'react';
import PatientsTable from '../tables/PatientsTable';
import TableActions from '../tables/TableActions';
import TableFilters from '../tables/TableFilters';
import UserIcon from '@material-ui/icons/Person';
import { Filter, PatientRowProps } from '../tables/interfaces';
import { patientsData } from '../../data/patients';
import { FilterChipProps } from '../common/interfaces';
import _ from 'lodash';

export default function PatientsContainer(): ReactElement {
  const [filters, setFilters] = useState<Array<Filter>>([]);
  const [tableData, setTableData] = useState<Array<PatientRowProps>>(patientsData);
  const [search, setSearch] = useState<string>('');

  const [preDefinedFilters, setPreDefinedFilters] = useState<Array<FilterChipProps>>([
    {
      color: '#3279A5',
      label: 'My Patients',
      icon: <UserIcon />,
      selected: false,
      filters: [
        {
          field: {
            label: 'Doctor',
            field: 'doctor',
          },
          value: {
            label: 'Charlie Curtis',
            value: '412351324',
          },
          filtersList: filters,
          setState: setFilters,
        },
      ],
      onClick: setFilters,
    },
  ]);

  useEffect(() => {
    let newData = patientsData;
    for (const f of filters) {
      newData = newData.filter((elem) => elem.doctor.id === f.value.value);
    }
    setTableData(newData);
  }, [filters, search]);

  useEffect(() => {
    for (let i = 0; i < preDefinedFilters.length; i++) {
      if (_.isEqual(filters, preDefinedFilters[i].filters)) {
        preDefinedFilters[i].selected = true;
      } else {
        preDefinedFilters[i].selected = false;
      }
    }
    setPreDefinedFilters(preDefinedFilters);
  }, [filters]);

  return (
    <>
      <TableActions
        preDefinedFilters={preDefinedFilters}
        primaryAction={{ label: 'New Patient', action: '' }}
      />
      <TableFilters
        filters={filters}
        onSearch={(e) => {
          setSearch(e.target.value);
        }}
      />
      <div>
        <PatientsTable data={tableData} />
      </div>
    </>
  );
}
