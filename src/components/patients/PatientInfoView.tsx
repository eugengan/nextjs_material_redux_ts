import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import PatientInfoSection from './PatientInfoSection';

const useStyles = makeStyles(({ spacing }: Theme) =>
  createStyles({
    root: {
      paddingTop: spacing(3),
      paddingBottom: spacing(3),
    },
  })
);

export default function PatientView(): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <PatientInfoSection
        heading="Patient"
        data={[
          { label: 'Patient ID', value: '1234567890' },
          { label: 'Email', value: 'barbaramcclintock@email.com' },
          { label: 'First Name', value: 'Barbara' },
          { label: 'Last Name', value: 'McClintock' },
          { label: 'Doctor', value: 'Michael Levy' },
          { label: 'Location', value: 'London, United Kingdom' },
          { label: 'Phone Number', value: '+44 567 85 244' },
          { label: 'Date of Birth', value: 'Oct 1, 1987' },
          { label: 'Personal ID', value: '5720457289489' },
        ]}
        avatar={{
          src: '/images/test-avatar-patient-01-large.png',
          initials: 'BM',
        }}
      />
      <PatientInfoSection
        heading="Partner"
        data={[
          { label: 'Patient ID', value: '1234567890' },
          { label: 'Email', value: 'abdulmcclintock@email.com' },
          { label: 'First Name', value: 'Abdul' },
          { label: 'Last Name', value: 'McClintock' },
          { label: 'Doctor', value: 'Michael Levy' },
          { label: 'Location', value: 'London, United Kingdom' },
          { label: 'Phone Number', value: '+44 567 85 123' },
          { label: 'Date of Birth', value: 'Feb 11, 1980' },
          { label: 'Personal ID', value: '5720457289567' },
        ]}
        avatar={{
          src: '',
          initials: 'AM',
        }}
      />
    </div>
  );
}
