import React, { ReactElement } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MoreIcon from '@material-ui/icons/MoreVert';
import Grid from '@material-ui/core/Grid';
import InfoField from './InfoField';
import { PatientInfoSectionProps } from './interfaces';

const useStyles = makeStyles(({ spacing, typography }: Theme) =>
  createStyles({
    root: {
      paddingTop: spacing(3),
      paddingBottom: spacing(3),
    },
    avatar: {
      width: spacing(13),
      height: spacing(13),
      fontSize: typography.pxToRem(24),
    },
    paper: {
      marginBottom: spacing(2),
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingBottom: 14,
    },
    headerText: {
      marginLeft: spacing(2),
    },
    details: {
      padding: 12,
    },
  })
);

export default function PatientViewSection(props: PatientInfoSectionProps): ReactElement {
  const classes = useStyles();

  const renderFields = (which: 'even' | 'odd') => {
    if (which === 'even')
      return props.data.map((elem, i) => {
        if (i % 2 === 0) return <InfoField label={elem.label} value={elem.value} />;
      });
    else
      return props.data.map((elem, i) => {
        if (i % 2 !== 0) return <InfoField label={elem.label} value={elem.value} />;
      });
  };

  return (
    <Paper className={classes.paper}>
      <div className={classes.header}>
        <Typography variant="h6" className={classes.headerText}>
          {props.heading}
        </Typography>
        <IconButton>
          <MoreIcon />
        </IconButton>
      </div>
      <Grid container spacing={8} className={classes.details}>
        <Grid item sm={2}>
          <Avatar src={props.avatar.src} className={classes.avatar}>
            {props.avatar.initials}
          </Avatar>
        </Grid>
        <Grid item sm={3}>
          {renderFields('even')}
        </Grid>
        <Grid item sm={3}>
          {renderFields('odd')}
        </Grid>
      </Grid>
    </Paper>
  );
}
