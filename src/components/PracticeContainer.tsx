import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import StyledTabs from './common/StyledTabs';

const useStyles = makeStyles(() =>
  createStyles({
    root: { flexGrow: 1 },
  })
);

export default function PracticeContainer(): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <StyledTabs
        links={[
          { name: 'Users', route: '/practice/users' },
          { name: 'Locations', route: '/practice/locations' },
          { name: 'Logos & Site', route: '/practice/logos-site' },
        ]}
      />
    </div>
  );
}
