import React, { ReactElement } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import StyledTabs from './common/StyledTabs';

const useStyles = makeStyles(() =>
  createStyles({
    root: { flexGrow: 1 },
  })
);

export default function LibraryContainer(): ReactElement {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <StyledTabs
        links={[
          { name: 'Modules', route: '/library/modules' },
          { name: 'Forms', route: '/library/forms' },
          { name: 'Form Packets', route: '/library/form-packets' },
        ]}
      />
    </div>
  );
}
