import { createMuiTheme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

// To-do Px to REM, breakpoints for fonts
// re-factor, breakdown, replace repeating colors with variables

const customPalette = {
  primary: {
    light: '#22AEB0',
    main: '#009D9E',
    dark: 'rgba(0,59,88,0.88)',
  },
  secondary: {
    main: '#19857b',
  },
  error: {
    main: '#DE5363',
  },
  warning: {
    main: '#F09E00',
  },
  info: {
    main: '#4573E3',
  },
  background: {
    default: '#ffffff',
  },
  text: {
    primary: '#003B58',
    secondary: '#4D768B',
    disabled: '#7F9CAB',
  },
};

const theme = createMuiTheme({
  typography: {
    fontFamily: '"Nunito Sans", sans-serif',
    fontSize: 14,
    fontWeightMedium: 600,
    h1: {
      fontWeight: 300,
      fontSize: 96,
      letterSpacing: -1.5,
      lineHeight: 'normal',
    },
    h2: {
      fontWeight: 400,
      fontSize: 60,
      letterSpacing: -0.25,
      lineHeight: 'normal',
    },
    h3: {
      fontWeight: 400,
      fontSize: 48,
      lineHeight: 'normal',
      letterSpacing: -0.1,
    },
    h4: {
      fontWeight: 600,
      fontSize: 34,
      letterSpacing: 0.25,
      lineHeight: 'normal',
    },
    h5: {
      fontWeight: 700,
      fontSize: 24,
      letterSpacing: 0.1,
      lineHeight: 'normal',
    },
    h6: {
      fontWeight: 700,
      fontSize: 20,
      letterSpacing: 0.15,
      lineHeight: 'normal',
    },
    subtitle1: {
      fontWeight: 700,
      fontSize: 18,
      lineHeight: 'normal',
      letterSpacing: 0.1,
    },
    subtitle2: {
      fontWeight: 700,
      fontSize: 16,
      letterSpacing: 0.1,
    },
    body1: {
      fontWeight: 600,
      fontSize: 16,
      letterSpacing: 0.1,
      lineHeight: '22px',
    },
    body2: {
      fontWeight: 600,
      fontSize: 14,
      letterSpacing: 0.1,
      lineHeight: 'normal',
    },
    button: {
      fontWeight: 700,
      fontSize: 14,
      letterSpacing: 0.1,
    },
    caption: {
      fontWeight: 600,
      fontSize: 13,
      letterSpacing: 0.1,
    },
    overline: {
      fontWeight: 600,
      fontsize: 12,
      letterSpacing: 1.2,
    },
  },
  palette: customPalette,
  overrides: {
    MuiInputAdornment: {
      root: {
        color: customPalette.text.disabled,
      },
    },
    MuiOutlinedInput: {
      notchedOutline: {
        borderColor: fade(customPalette.text.primary, 0.21),
      },
      inputMarginDense: {
        fontSize: 14,
        letterSpacing: 0.1,
        paddingTop: 8,
        paddingBottom: 8,
      },
    },
    MuiButton: {
      root: {
        textTransform: 'none',
        boxShadow: 'none',
        padding: '8.5px 16px',
        minWidth: 'auto',
      },
      textPrimary: {
        color: customPalette.primary.light,
      },
      startIcon: {
        marginLeft: '-8px',
        marginRight: '4px',
        color: customPalette.primary.light,
      },
      endIcon: {
        marginLeft: '4px',
        marginRight: '-8px',
        color: '#EDEDED',
      },
      contained: {
        boxShadow: 'none',
        '&:hover': {
          boxShadow: 'none',
        },
      },
      containedPrimary: {
        backgroundColor: customPalette.primary.light,
        '&:hover': {
          backgroundColor: customPalette.primary.main,
        },
      },
      outlined: {
        padding: '9px 16px',
        borderColor: '#CFD9DF',
        '&:hover': {
          backgroundColor: '#F2F5F7',
        },
        '& $startIcon': {
          color: customPalette.primary.light,
        },
        '& $endIcon': {
          color: '#66899B',
        },
      },
      sizeLarge: {
        fontSize: 16,
        padding: '14px 24px',
      },
    },
    MuiAvatar: {
      root: {
        width: 48,
        height: 48,
        fontSize: 14,
        fontWeight: 700,
        letterSpacing: 0.1,
      },
      colorDefault: {
        backgroundColor: '#E6EBEE',
        color: '#4D768B',
        border: '1px solid #FFFFFF',
      },
    },
    MuiTableRow: {
      root: {
        verticalAlign: 'top',
      },
    },
    MuiTableCell: {
      head: {
        padding: '10px 8px 8px',
        color: '#7F9CAB',
        fontWeight: 600,
        fontSize: 13,
      },
      body: {
        padding: '16px 6px',
      },
    },
    MuiChip: {
      root: {
        color: '#fff',
        borderRadius: 40,
        backgroundColor: '#CFD9DF',
        margin: '0 4px',
      },
      label: {
        paddingLeft: 8,
        paddingRight: 12,
      },
      icon: {
        marginLeft: 8,
        marginRight: 0,
        borderRadius: '100%',
        padding: 3,
        fontSize: 20,
        color: '#CFD9DF',
        backgroundColor: '#fff',
      },
      outlined: {
        color: fade('#003B58', 0.88),
        borderColor: '#CFD9DF',
        '& $icon': {
          marginLeft: 8,
          color: '#fff',
          backgroundColor: '#CFD9DF',
        },
      },
      iconColorPrimary: {
        color: customPalette.primary.main,
        background: '#fff',
      },
      iconColorSecondary: {
        color: customPalette.secondary.main,
        background: '#fff',
      },
      sizeSmall: {
        borderRadius: 4,
        height: 32,
        '&$colorPrimary': {
          color: customPalette.primary.main,
          backgroundColor: '#E7F6F7',
        },
        '& $deleteIconColorPrimary': {
          color: customPalette.primary.main,
          width: 14,
          height: 14,
          marginRight: 10,
        },
      },
    },
    MuiIconButton: {
      root: {
        color: '#7F9CAB',
        width: 32,
        height: 32,
      },
    },
    MuiLinearProgress: {
      root: {
        height: 12,
        minWidth: 180,
        borderRadius: 20,
        backgroundColor: '#1a90ff',
      },
      colorPrimary: {
        backgroundColor: '#E6EBEE',
      },
      bar: {
        backgroundColor: '#1a90ff',
      },
    },
    MuiPaper: {
      root: {
        padding: 24,
      },
      elevation1: {
        boxShadow: '0 1px 4px rgba(0,0,0,0.24)',
      },
    },
  },
});

export default theme;
