import React, { ReactElement } from 'react';
import FormsContainer from '../src/components/forms/FormsContainer';
import HomeLayout from '../src/components/layouts/HomeLayout';

function Forms(): ReactElement {
  return <FormsContainer />;
}

Forms.Layout = HomeLayout;
export default Forms;
