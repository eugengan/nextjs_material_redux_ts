import React, { ReactElement } from 'react';
import OverviewContainer from '../src/components/overview/OverviewContainer';
import HomeLayout from '../src/components/layouts/HomeLayout';

function Index(): ReactElement {
  return <OverviewContainer />;
}

Index.Layout = HomeLayout;
export default Index;
