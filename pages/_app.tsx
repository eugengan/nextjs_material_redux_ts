import React, { ReactElement } from 'react';
import Head from 'next/head';
import { AppProps } from 'next/app';
import { ThemeProvider } from '@material-ui/core/styles';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../src/theme';
import MainNav from '../src/components/MainNav';

const useStyles = makeStyles(
  createStyles({
    root: {
      display: 'flex',
      backgroundColor: theme.palette.background.default,
    },
    nav: {
      flexShrink: 0,
      width: 260,
    },
    content: {
      flexGrow: 1,
      padding: `${theme.spacing(7)}px ${theme.spacing(6)}px`,
    },
  })
);

type IAppProps = AppProps & {
  Component: {
    Layout: React.ElementType;
  };
};

export default function MyApp(props: IAppProps): ReactElement {
  const classes = useStyles(theme);
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      (jssStyles.parentElement as HTMLElement).removeChild(jssStyles);
    }
  }, []);

  const Noop = ({ children }: { children: React.ElementType | React.ElementType[] }) => children;
  const Layout = Component.Layout || Noop;

  return (
    <React.Fragment>
      <Head>
        <title>EngagedMD Staff Portal</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <div className={classes.root}>
          <nav className={classes.nav}>
            <MainNav />
          </nav>
          <main className={classes.content}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </main>
        </div>
      </ThemeProvider>
    </React.Fragment>
  );
}
