import React, { ReactElement } from 'react';
import PatientsContainer from '../src/components/patients/PatientsContainer';
import HomeLayout from '../src/components/layouts/HomeLayout';

function Patients(): ReactElement {
  return <PatientsContainer />;
}

Patients.Layout = HomeLayout;
export default Patients;
