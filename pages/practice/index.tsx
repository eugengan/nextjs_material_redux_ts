import React, { ReactElement } from 'react';
import PracticeContainer from '../../src/components/PracticeContainer';

export default function Index(): ReactElement {
  return <PracticeContainer />;
}
