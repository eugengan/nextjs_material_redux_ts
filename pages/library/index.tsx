import React, { ReactElement } from 'react';
import LibraryContainer from '../../src/components/LibraryContainer';

export default function Index(): ReactElement {
  return <LibraryContainer />;
}
