import React, { ReactElement } from 'react';
import PatientView from '../../src/components/patients/PatientView';
import PatientLayout from '../../src/components/layouts/PatientLayout';

function PatientIndex(): ReactElement {
  return <PatientView />;
}

PatientIndex.Layout = PatientLayout;
export default PatientIndex;
