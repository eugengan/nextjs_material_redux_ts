import React, { ReactElement } from 'react';
import PatientInfoView from '../../src/components/patients/PatientInfoView';
import PatientLayout from '../../src/components/layouts/PatientLayout';

function PatientInfo(): ReactElement {
  return <PatientInfoView />;
}

PatientInfo.Layout = PatientLayout;
export default PatientInfo;
