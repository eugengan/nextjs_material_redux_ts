import React, { ReactElement } from 'react';
import ModulesContainer from '../src/components/modules/ModulesContainer';
import HomeLayout from '../src/components/layouts/HomeLayout';

function Modules(): ReactElement {
  return <ModulesContainer />;
}

Modules.Layout = HomeLayout;
export default Modules;
